
class Calculator {

    constructor() {
        this._stack = []
    }
    
    push(val) {
        this._stack.push(val)
    }

    invoke(operation) {
        if (this._stack.length < 2)
            throw new Error()
        var v1 = this._stack.pop()
        var v2 = this._stack.pop()

        this._stack.push(operation(v2,v1))
    }

    get stack() {
        return this._stack
    }
}

module.exports = Calculator