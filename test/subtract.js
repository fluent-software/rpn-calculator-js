var chai = require('chai'),
    subtract = require('../src/subtract')

chai.should()

describe('The subtract operation', function() {
    it('should correctly subtract two values, returning the result of the calculation', function() {
        var result = subtract(10,8)
        result.should.equal(2)
    })
})