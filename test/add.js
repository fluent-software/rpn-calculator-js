var chai = require('chai'),
    add = require('../src/add')

chai.should()

describe('The add operation', function() {
    it('should correctly add two values, returning the result of the addition', function() {
        var result = add(2,3)
        result.should.equal(5)
    })
})