var chai = require('chai'),
    Calculator = require('../src/index')

var should = chai.should();

describe('A new calculator', function() {

    var calculator

    beforeEach(function() {
        calculator = new Calculator()
    })
    
    it('should have an empty stack when just created', function() {
        calculator.stack.should.eql([])
    })

    it('should have a stack size of 1 after pushing a new value', function() {
        calculator.push(5)
        calculator.stack.should.have.length(1)
    })

    it('should have a stack containing the value pushed after pushing a new value', function() {
        calculator.push(2)
        calculator.stack[0].should.equal(2)
    })

    it('should not allow an operation to be invoked when no values have been pushed', function() {
        should.Throw(calculator.invoke, Error)
    })

})

describe('A calculator with a single item on the stack', function() {
    
    var calculator

    beforeEach(function() {
        calculator = new Calculator()
        calculator.push(3)
    })
    
    it('should contain two values in the correct order after pushing a new value', function() {
        calculator.push(4)
        calculator.stack.should.eql([3,4])
    })

    it('should not allow an operation to be invoked when no other values have been pushed', function() {
        should.Throw(calculator.invoke, Error)
    })
})

describe('A calculator with two values on the stack', function() {

    var calculator

    beforeEach(function() {
        calculator = new Calculator()
        calculator.push(2)
        calculator.push(5)
    })

    it('should have a stack containing a single value after applying an operation', function() {
        calculator.invoke(function(){})
        calculator.stack.should.have.length(1)
    })

    it('should have a stack containing the correct result of the calculation after applying an operation', function() {
        calculator.invoke(function() {return -3})
        calculator.stack[0].should.equal(-3)
    })
    
})

describe('A calculator with three values on the stack', function() {

    var calculator

    beforeEach(function() {
        calculator = new Calculator()
        calculator.push(10)
        calculator.push(11)
        calculator.push(12)
    })


    it('should not have altered the first value on the stack after applying an operation', function() {
        calculator.invoke(function() {return 23})
        calculator.stack.should.eql([10,23])
    })

})